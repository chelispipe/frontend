import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'

// roles
import Dashboard from '../views/roles/Dashboard.vue';
import Editar from '../views/roles/Editar.vue';
import Nuevo from '../views/roles/Nuevo.vue';

// asociados
import AsociadoLista from '../views/asociados/Dashboard.vue';
import EditarAsociado from '../views/asociados/Editar.vue';
import NuevoAsociado from '../views/asociados/Nuevo.vue';

 // creditos
import CreditosLista from '../views/creditos/Dashboard.vue';
import EditarCredito from '../views/creditos/Editar.vue';
import NuevoCredito from '../views/creditos/Nuevo.vue';

// fondos
import FondosLista from '../views/fondos/Dashboard.vue';
import EditarFondo from '../views/fondos/Editar.vue';
import NuevoFondo from '../views/fondos/Nuevo.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/roles',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/roles/editar/:id',
    name: 'Editar',
    component: Editar
  },
  {
    path: '/roles/nuevo',
    name: 'Nuevo',
    component: Nuevo
  },
  {
    path: '/asociados',
    name: 'AsociadoLista',
    component: AsociadoLista
  },
  {
    path: '/asociados/editar/:id',
    name: 'EditarAsociado',
    component: EditarAsociado
  },
  {
    path: '/asociados/nuevo',
    name: 'NuevoAsociado',
    component: NuevoAsociado
  },
  {
    path: '/creditos',
    name: 'CreditosLista',
    component: CreditosLista
  },
  {
    path: '/creditos/editar/:id',
    name: 'EditarCredito',
    component: EditarCredito
  },
  {
    path: '/creditos/nuevo',
    name: 'NuevoCredito',
    component: NuevoCredito
  },
  {
    path: '/fondos',
    name: 'FondosLista',
    component: FondosLista
  },
  {
    path: '/fondos/editar/:id',
    name: 'EditarFondo',
    component: EditarFondo
  },
  {
    path: '/fondos/nuevo',
    name: 'NuevoFondo',
    component: NuevoFondo
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
